/**
 * Script responsável por mover arquivos de um diretório raiz com subpastas para uma pasta única.
 * Created by jefferson on 05/10/16.
 */
def diretorioBase = "/home/ati.old/Documentos/CP-Pro2/1"
def diretorioDestino = "/home/ati.old/Pasta/"

new File(diretorioBase).eachFile {
    copiar(it,diretorioDestino)
}
/*
* Método responsável por Mover os arquivos para um Diretório especifico
*/
def copiar(path,destino) {
    if (path.isFile())
    {
        def moverArquivo = path.renameTo(new File(new File(destino), path.getName()))

        if (moverArquivo)
            println("O ARQUIVO: " + path.getName() + " foi movido com sucesso!")
        else
            println("O ARQUIVO: " + path.getName() + " NÃO foi movido!")
    }
    else
    {
        (path).eachFile { arquivo ->
            copiar(arquivo, destino)
        }
    }
}